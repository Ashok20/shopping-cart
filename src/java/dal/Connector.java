
package dal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author ASHOK
 */
public class Connector {
    
    private static String url = "jdbc:mysql://localhost:3306/nikedb";  
    private static String driverName = "com.mysql.jdbc.Driver";   
    private static String username = "root";   
    private static String password = "";
    
    public static Connection getConnection() throws SQLException, ClassNotFoundException{
        Class.forName(driverName);
        return (Connection)DriverManager.getConnection(url,username,password);
    }

}
