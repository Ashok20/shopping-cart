
package control;

import dal.Connector;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Product;


public class ControlProduct {

    private static PreparedStatement stmt = null;
    private static String sql = null;
    private static Connection con = null;

    
    
    public static int addProduct(Product input) throws SQLException, ClassNotFoundException{
        
        sql = "INSERT INTO product(pname,pdetail,pcost,pprice,ptype,ppath) VALUES(?,?,?,?,?,?)";
        
        con = Connector.getConnection();
        stmt = con.prepareStatement(sql);
        stmt.setString(1, input.getName());
        stmt.setString(2, input.getDetail());
        stmt.setDouble(3, input.getCost());
        stmt.setDouble(4, input.getPrice());
        stmt.setString(5, input.getType());
        
        //Image Path
        stmt.setString(6, "");
          
        int status = stmt.executeUpdate();
        
        if (con != null)
            con.close();
        
        return status;
    }
    
    
    public static Product getProduct(int pid) throws SQLException, ClassNotFoundException{
        
        sql = "SELECT * FROM product WHERE pid = ?";
        
        con = Connector.getConnection();
        stmt = con.prepareStatement(sql);
        stmt.setInt(1, pid);
        
        ResultSet rs = stmt.executeQuery();
        Product productObj = null;        
        
        while(rs.next()){
            productObj = new Product(rs.getInt("pid"),rs.getString("pname"),rs.getString("pdetail"),rs.getDouble("pcost"),rs.getDouble("pprice"),rs.getString("ptype"),rs.getString("ppath"));
        }
        
        if (con != null)
            con.close();
        
        return productObj;
    } 
    
    
    public static ArrayList<Product> getProducts() throws SQLException, ClassNotFoundException{
        
        sql = "SELECT * FROM product";
        
        con = Connector.getConnection();
        stmt = con.prepareStatement(sql);
        
        ResultSet rs = stmt.executeQuery();
        ArrayList<Product> objArray = new ArrayList<>();        
        int i = 0;
        while(rs.next()){          
            objArray.add(i, new Product(rs.getInt("pid"), rs.getString("pname"), rs.getString("pdetail"), rs.getDouble("pcost"), rs.getDouble("pprice"), rs.getString("ptype"), rs.getString("ppath")));
            i++;
        }  
        
        if (con != null)
            con.close();
        
        return objArray;
    }


    public static ArrayList<Product> getMultiProducts(String input) throws SQLException, ClassNotFoundException{
        
        con = Connector.getConnection();
        stmt = con.prepareStatement(input);
        
        ResultSet rs = stmt.executeQuery();
        ArrayList<Product> objArray = new ArrayList<>();        
        int i = 0;
        while(rs.next()){          
            objArray.add(i, new Product(rs.getInt("pid"), rs.getString("pname"), rs.getString("pdetail"), rs.getDouble("pcost"), rs.getDouble("pprice"), rs.getString("ptype"), rs.getString("ppath")));
            i++;
        }  
        
        if (con != null)
            con.close();
        
        return objArray;
    }
    
    public static int maxId() throws SQLException, ClassNotFoundException{

        sql = "SELECT MAX(pid) AS pid FROM product";
        int id = -1;
        
        con = Connector.getConnection();
        PreparedStatement create = con.prepareStatement(sql);
        
        ResultSet rs;
        rs = create.executeQuery();
        
        while(rs.next()){
            id =rs.getInt("pid");
        }       
        
        if (con != null)
            con.close();
        
        return id;
    }
    
    
    public static int updateProduct(Product input) throws SQLException, ClassNotFoundException{
        
        sql = "UPDATE product SET pname=?, pdetail=?, pcost=?, pprice=? WHERE pid=?";
        
        con = Connector.getConnection();
        stmt = con.prepareStatement(sql);
        stmt.setString(1, input.getName());
        stmt.setString(2, input.getDetail());
        stmt.setDouble(3, input.getCost());
        stmt.setDouble(4, input.getPrice());
        stmt.setInt(5, input.getId());
          
        int status = stmt.executeUpdate();
        
        if (con != null)
            con.close();
        
        return status;
    }
    
    
    public static int deleteProduct(int productId) throws SQLException, ClassNotFoundException{
        sql = "DELETE FROM product WHERE pid=?";
        
        con = Connector.getConnection();
        stmt = con.prepareStatement(sql);
        stmt.setInt(1, productId);
        
        int status = stmt.executeUpdate();
        
        if (con != null)
            con.close();
        
        return status;
    }
    
    
}
