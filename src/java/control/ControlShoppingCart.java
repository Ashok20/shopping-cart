
package control;

import dal.Connector;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.ShoppingCart;

/**
 *
 * @author ASHOK
 */
public class ControlShoppingCart {

    private static PreparedStatement stmt = null;
    private static String sql = null;
    private static Connection con = null;
    
    public static int addToShoppingCart(ShoppingCart input) throws SQLException, ClassNotFoundException{
        
        sql = "INSERT INTO shoppingcart(pid,qty,price,ucid) VALUES(?,?,?,?)";
        
        con = Connector.getConnection();
        stmt = con.prepareStatement(sql);
        stmt.setInt(1, input.getPid());
        stmt.setInt(2, input.getQty());
        stmt.setDouble(3, input.getPrice());
        stmt.setInt(4, input.getUcid());
          
        int status = stmt.executeUpdate();
        
        if (con != null)
            con.close();
        
        return status;
    }
    
    
    public static ArrayList<ShoppingCart> getShoppingCartProducts(int userCartId) throws SQLException, ClassNotFoundException{
        
        sql = "SELECT * FROM shoppingcart WHERE ucid = ?";
        
        con = Connector.getConnection();
        stmt = con.prepareStatement(sql);
        stmt.setInt(1, userCartId);
        
        ResultSet rs = stmt.executeQuery();
        ArrayList<ShoppingCart> objArray = new ArrayList<>();        
        int i = 0;
        while(rs.next()){          
            objArray.add(i, new ShoppingCart(rs.getInt("cartid"), rs.getInt("pid"), rs.getInt("qty"), rs.getDouble("price")));
            i++;
        }  
        
        if (con != null)
            con.close();
        
        return objArray;
    }
    
    public static int deleteFromShoppingCartProducts(int ucid) throws SQLException, ClassNotFoundException{
        
        sql = "DELETE FROM shoppingcart WHERE ucid = ?";
        
        con = Connector.getConnection();
        stmt = con.prepareStatement(sql);
        stmt.setInt(1, ucid);
     
        int status = stmt.executeUpdate();
        
        if (con != null)
            con.close();
        
        return status;
    }
    
    
    public static int deleteSingleProduct(int ucid, int pid) throws SQLException, ClassNotFoundException{
        
        sql = "DELETE FROM shoppingcart WHERE ucid = ? AND pid = ?";
        
        con = Connector.getConnection();
        stmt = con.prepareStatement(sql);
        stmt.setInt(1, ucid);
        stmt.setInt(2, pid);
     
        int status = stmt.executeUpdate();
        
        if (con != null)
            con.close();
        
        return status;
    }
    
    
}
