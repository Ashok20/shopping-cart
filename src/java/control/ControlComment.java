
package control;

import dal.Connector;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import model.Comment;

/**
 *
 * @author ASHOK
 */

public class ControlComment {

    private static PreparedStatement stmt = null;
    private static String sql = null;
    private static Connection con = null;
    
    
    public static int addComment(Comment input) throws SQLException, ClassNotFoundException {

        sql = "INSERT INTO comment(pid,comment,rating,cdate) VALUES(?,?,?,?)";

        con = Connector.getConnection();
        stmt = con.prepareStatement(sql);
        stmt.setInt(1, input.getProductId());
        stmt.setString(2, input.getComment());
        stmt.setInt(3, input.getRating());
        stmt.setString(4, input.getCommentDate());
        int status = stmt.executeUpdate();

        if (con != null)
            con.close();

        return status;
    }
    
    
    public static Comment getComment(int commentId) throws SQLException, ClassNotFoundException{
        
        sql = "SELECT * FROM comment WHERE cid = ?";
        con = Connector.getConnection();
        stmt = con.prepareStatement(sql);
        stmt.setInt(1, commentId);
        
        ResultSet rs = stmt.executeQuery();
        Comment commentObj = null;        
        
        while(rs.next()){
            commentObj = new Comment(rs.getInt("pid"),rs.getString("comment"),rs.getInt("rating"),rs.getString("cdate"));
        }        

        if (con != null)
            con.close();
        
        return commentObj;
    } 
    
    
    public static ArrayList<Comment> getComments() throws SQLException, ClassNotFoundException{
        
        sql = "SELECT * FROM comment";
        con = Connector.getConnection();
        stmt = con.prepareStatement(sql);
        
        ResultSet rs = stmt.executeQuery();
        ArrayList<Comment> objArray = new ArrayList<>();        
        int i = 0;
        while(rs.next()){
            objArray.add(i, new Comment(rs.getInt("pid"),rs.getString("comment"),rs.getInt("rating"),rs.getString("cdate")));
            i++;
        }  
        
        if (con != null)
            con.close();
        
        return objArray;
    }


    public static ArrayList<Comment> getMultiComments(int productId) throws SQLException, ClassNotFoundException{
        
        sql = "SELECT * FROM comment WHERE pid = ?";
        con = Connector.getConnection();
        stmt = con.prepareStatement(sql);
        stmt.setInt(1, productId);
               
        ResultSet rs = stmt.executeQuery();
        ArrayList<Comment> objArray = new ArrayList<>();        
        int i = 0;
        while(rs.next()){          
            objArray.add(i, new Comment(rs.getString("comment"),rs.getInt("rating"),rs.getString("cdate")));
            i++;
        }  
        
        if (con != null)
            con.close();
        
        return objArray;
    }
   
    
    public static String commentDate(){

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MMMM/dd");
        LocalDate localDate = LocalDate.now();       
        return localDate.toString();
    }
   
}
