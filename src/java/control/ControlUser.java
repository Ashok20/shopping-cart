
package control;

import dal.Connector;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.User;

/**
 *
 * @author ASHOK
 */
public class ControlUser {
    
    private static PreparedStatement stmt = null;
    private static String sql = null;
    private static Connection con = null;
    
    public static int addUser(User input) throws SQLException, ClassNotFoundException{
        
        sql = "INSERT INTO user(fullname,username,password,email,type) VALUES(?,?,?,?,?)";
        
        con = Connector.getConnection();
        stmt = con.prepareStatement(sql);
        stmt.setString(1, input.getFullName());
        stmt.setString(2, input.getUserName());
        stmt.setString(3, input.getPassword());
        stmt.setString(4, input.getEmail());
        stmt.setString(5, input.getType());
        
        int status = stmt.executeUpdate();
        
        if (con != null)
            con.close();
        
        return status;
    }
    
    
    public static User getUser(String userName) throws SQLException, ClassNotFoundException{
        
        sql = "SELECT * FROM user WHERE username = ?";
        
        con = Connector.getConnection();
        stmt = con.prepareStatement(sql);
        stmt.setString(1, userName);
        
        ResultSet rs = stmt.executeQuery();
        User userObj = null;        
        
        while(rs.next()){
            userObj = new User(rs.getInt("uid"),rs.getString("fullname"),rs.getString("username"),rs.getString("password"),rs.getString("email"),rs.getString("type"));
        }
        
        if (con != null)
            con.close();
        
        return userObj;
    } 
    
    
    public static ArrayList<User> getUsers(String userName, String pass) throws SQLException, ClassNotFoundException{
                
        sql = "SELECT * FROM user WHERE username = ? AND password = ?";
        
        con = Connector.getConnection();
        stmt = con.prepareStatement(sql);
        stmt.setString(1, userName);
        stmt.setString(2, pass);
        
        ResultSet rs = stmt.executeQuery();
        ArrayList<User> objArray = new ArrayList<>();        
        int i = 0;
        while(rs.next()){          
            objArray.add(i, new User(rs.getInt("uid"),rs.getString("fullname"),rs.getString("username"),rs.getString("password"),rs.getString("email"),rs.getString("type")));
            i++;
        }  
        
        if (con != null)
            con.close();
        
        return objArray;
    }    
    
    
    public static int updateUser() throws SQLException, ClassNotFoundException{
        return 0;
    }
    
    
    public static void deleteUser() throws SQLException, ClassNotFoundException{
        
    }
    
    
}
