
package control;

import dal.Connector;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.UserCart;

/**
 *
 * @author ASHOK
 */
public class ControlUserCart {
    private static PreparedStatement stmt = null;
    private static String sql = null;
    private static Connection con = null;
    
    
    public static int addUserCart(int uid) throws SQLException, ClassNotFoundException{
        
        sql = "INSERT INTO usercart(uid) VALUES(?)";
        
        con = Connector.getConnection();
        stmt = con.prepareStatement(sql);
        stmt.setInt(1, uid);
        int status = stmt.executeUpdate();
        
        if (con != null)
            con.close();
          
        return status;
    }
    
    
    public static UserCart getUserCartInfo(int uid) throws SQLException, ClassNotFoundException{
        
        sql = "SELECT * FROM usercart WHERE uid = ?";
        con = Connector.getConnection();
        stmt = con.prepareStatement(sql);
        stmt.setInt(1, uid);
        
        ResultSet rs = stmt.executeQuery();
        UserCart userCartObj = null;      

        while(rs.next()){          
            userCartObj = new UserCart(rs.getInt("ucid"), rs.getInt("uid"));
        }  
        
        if (con != null)
            con.close();
        
        return userCartObj;
    }
    
    
    public static int deleteFromUserCart(int uid) throws SQLException, ClassNotFoundException{
        
        sql = "DELETE FROM usercart WHERE uid = ?";
        con = Connector.getConnection();
        stmt = con.prepareStatement(sql);
        stmt.setInt(1, uid);
        
        int status = stmt.executeUpdate();
        
        if (con != null)
            con.close();
             
        return status;
    }        
    
}
