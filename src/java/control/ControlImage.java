
package control;

import dal.Connector;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;


public class ControlImage {
    
    private static final String UPLOAD_DIRECTORY = "D:/@NSBM/2ns Year/2n Semester/J2EE/Poject/ShoeMart/web/duplicateImage";
    private static Connection con = null;
 
    
    public static String renameImage(){
        
        UUID uuid = UUID.randomUUID();
        String randomImgName = uuid.toString();    
        return randomImgName;
    }
    
    
    public static int duplicateImg(HttpServletRequest request) throws SQLException, ClassNotFoundException, Exception{

        ControlImage imgObj = new ControlImage();
        String imgPath = null;

        List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
        for(FileItem item : multiparts){
            if(!item.isFormField()){
                //String name = new File(item.getName()).getName();
                String name = renameImage();
                item.write(new File(UPLOAD_DIRECTORY + File.separator + name + ".jpg"));
                imgPath = name + ".jpg";
            }
        }        
        return addImage(imgPath);
    }
    

    private static int addImage(String imgpath) throws SQLException, ClassNotFoundException{
        
        int status;
        int maxId = ControlProduct.maxId();
        String sql = "UPDATE product SET ppath = ? WHERE pid = ?";
        
        con = Connector.getConnection();
        PreparedStatement stmt = con.prepareStatement(sql);
        
        stmt.setString(1,imgpath);
        stmt.setInt(2,maxId);
        
        status = stmt.executeUpdate();
        
        if (con != null)
            con.close();
        
        return status;
    }
    
    
    public static boolean deleteImage(String fileName) throws IOException{
        File file = new File(UPLOAD_DIRECTORY+"/"+fileName);        
        return file.delete();
    }

    
    public static void replaceImage(HttpServletRequest request) throws SQLException, ClassNotFoundException, Exception{

        int productId = Integer.parseInt(request.getParameter("productId"));        
        String imageName = ControlProduct.getProduct(productId).getPath();
        
        List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
        for(FileItem item : multiparts){
            if(!item.isFormField()){
                item.write(new File(UPLOAD_DIRECTORY + File.separator + imageName + ".jpg"));
            }
        }        
    }
    
        
    //Method Overloading
    public static java.awt.Image reSizeImage(BufferedImage input){
        java.awt.Image imgEdited = input.getScaledInstance(240, 240,java.awt.Image.SCALE_SMOOTH);
        return imgEdited;
    }
    
    public static java.awt.Image reSizeImage(BufferedImage input, int width, int hight){
        java.awt.Image imgEdited = input.getScaledInstance(width, hight,java.awt.Image.SCALE_SMOOTH);
        return imgEdited;
    }
    
  
    
}
