
package servlet;

import control.ControlUser;
import control.ControlUserCart;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.User;
import model.UserCart;

/**
 *
 * @author ASHOK
 */
@WebServlet(name = "UserAuthentication", urlPatterns = {"/UserAuthentication"})
public class UserAuthentication extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UserAuthentication</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UserAuthentication at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ArrayList<User> arrayObj = new ArrayList<>();
        try {
            arrayObj = ControlUser.getUsers(request.getParameter("userName"), request.getParameter("password"));
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(UserAuthentication.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        if(arrayObj.size() == 1){
            HttpSession session = request.getSession();
            HttpSession ucidSession = request.getSession();
            //User userObj = new User();
            try {
                User userObj = ControlUser.getUser(request.getParameter("userName"));
                session.setAttribute("userId",userObj.getUid());
            } catch (SQLException | ClassNotFoundException ex) {
                Logger.getLogger(UserAuthentication.class.getName()).log(Level.SEVERE, null, ex);
            }
            int userId = (Integer)session.getAttribute("userId");
            
            try {
                UserCart userCartObj = ControlUserCart.getUserCartInfo(userId);
                
                if(userCartObj != null){
                    ucidSession.setAttribute("ucid", userCartObj.getUcid());
                }               
            } catch (SQLException | ClassNotFoundException ex) {
                Logger.getLogger(UserAuthentication.class.getName()).log(Level.SEVERE, null, ex);
            }
                    
            session.setAttribute("userName",request.getParameter("userName"));
            session.setAttribute("userType", arrayObj.get(0).getType());
            
            if(session.getAttribute("pidFromViewProduct") != null){ 
                String productId = session.getAttribute("pidFromViewProduct").toString();
                response.sendRedirect("viewProduct.jsp?productId=" + URLEncoder.encode(productId, "UTF-8"));
            }
            else{
                response.sendRedirect("index.jsp");
            }
        }
        else{
            String message = "* Incorrect user name or password";
            response.sendRedirect("login.jsp?message=" + URLEncoder.encode(message, "UTF-8"));
        }        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
