
package servlet;

import control.ControlProduct;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Product;

/**
 *
 * @author ASHOK
 */
@WebServlet(name = "EditProductProcess", urlPatterns = {"/EditProductProcess"})
public class EditProductProcess extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException { 

    }
   

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int productId = Integer.parseInt(request.getParameter("productId"));
        String productName = request.getParameter("productName");
        String productDetail = request.getParameter("productDetail");
        Double productCost = Double.parseDouble(request.getParameter("productCost"));
        Double productPrice = Double.parseDouble(request.getParameter("productPrice"));
        
        
        
        Product productObj = new Product(productId,productName,productDetail,productCost,productPrice);
        int status = 0;
        try {
            status = ControlProduct.updateProduct(productObj);
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(EditProductProcess.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(status == 0){
            request.setAttribute("message","Sorry, this item could not be updated");
            request.setAttribute("productId",productId);
        }
        else{
            request.setAttribute("message","Item Updated");
            request.setAttribute("productId",productId);
        }

        request.getRequestDispatcher("viewProduct.jsp").forward(request, response);
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
