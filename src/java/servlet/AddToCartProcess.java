
package servlet;

import control.ControlProduct;
import control.ControlShoppingCart;
import control.ControlUserCart;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Product;
import model.ShoppingCart;

/**
 *
 * @author ASHOK
 */
@WebServlet(name = "AddToCartProcess", urlPatterns = {"/AddToCartProcess"})
public class AddToCartProcess extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddToCartProcess</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddToCartProcess at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        
        //        HttpSession session = request.getSession();        
//        int userId = (Integer)session.getAttribute("userId");
//        int pid = Integer.parseInt(request.getParameter("txtProductId"));
//        try {
//            Product x = ControlProduct.getProduct(pid);
//            ShoppingCart shObj;
//            shObj = new ShoppingCart(pid,1,x.getPrice(),1);
//            ControlShoppingCart.addToShoppingCart(shObj);
//        } catch (SQLException | ClassNotFoundException ex) {
//            Logger.getLogger(AddToCartProcess.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        response.sendRedirect("cart.jsp");
        
        //ControlShoppingCart.addToShoppingCart(input);
        
        
        
        HttpSession session = request.getSession();
        HttpSession ucidSession = request.getSession();
        String pid = request.getParameter("txtProductId");
        //String userName =(String)session.getAttribute("userName");
        
        if(session.getAttribute("userName") == null){
            session.setAttribute("pidFromViewProduct", pid);
            response.sendRedirect("login.jsp");
        }
        else{
            int userId = (Integer)session.getAttribute("userId");
            if(ucidSession.getAttribute("ucid") == null){
                try {
                    ControlUserCart.addUserCart(userId);
                    int ucid = ControlUserCart.getUserCartInfo(userId).getUcid();
                    ucidSession.setAttribute("ucid", ucid);                    
                } catch (SQLException | ClassNotFoundException ex) {
                    Logger.getLogger(AddToCartProcess.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            int ucid = (Integer)ucidSession.getAttribute("ucid");
            try {
                Product productObj = ControlProduct.getProduct(Integer.parseInt(pid));
                ShoppingCart shoppingCartObj = new ShoppingCart(Integer.parseInt(pid),1,productObj.getPrice(),ucid);
                ControlShoppingCart.addToShoppingCart(shoppingCartObj);
            } catch (SQLException | ClassNotFoundException ex) {
                Logger.getLogger(AddToCartProcess.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            //PrintWriter out = response.getWriter();
            //out.println(request.getParameter("txtProductId"));
            
            response.sendRedirect("cart.jsp");
            
            
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
