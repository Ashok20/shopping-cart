
package model;

/**
 *
 * @author ASHOK
 */
public class User {
    
    private int uid;
    private String fullName;
    private String userName;
    private String password;
    private String email;
    private String type;


    public User() {
    }

    public User(int uid, String fullName, String userName, String password, String email, String type) {
        this.uid = uid;
        this.fullName = fullName;
        this.userName = userName;
        this.password = password;
        this.email = email;
        this.type = type;
    }

    public User(String fullName, String userName, String password, String email, String type) {
        this.fullName = fullName;
        this.userName = userName;
        this.password = password;
        this.email = email;
        this.type = type;
    }

    public User(String fullName, String userName, String password, String email) {
        this.fullName = fullName;
        this.userName = userName;
        this.password = password;
        this.email = email;
    }
    

    public void setUid(int uid) {
        this.uid = uid;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getUid() {
        return uid;
    }

    public String getFullName() {
        return fullName;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getType() {
        return type;
    }
    
}
