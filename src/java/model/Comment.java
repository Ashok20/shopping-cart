
package model;


/**
 *
 * @author ASHOK
 */
public class Comment {    
    
    private int commentId;
    private int productId;
    private String comment;
    private int rating;
    private String commentDate;

    public Comment() {
    }

    public Comment(int commentId, int productId, String comment, int rating, String commentDate) {
        this.commentId = commentId;
        this.productId = productId;
        this.comment = comment;
        this.rating = rating;
        this.commentDate = commentDate;
    }

    public Comment(int productId, String comment, int rating, String commentDate) {
        this.productId = productId;
        this.comment = comment;
        this.rating = rating;
        this.commentDate = commentDate;
    }
    
    public Comment(String comment, int rating, String commentDate) {
        this.comment = comment;
        this.rating = rating;
        this.commentDate = commentDate;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public void setCommentDate(String commentDate) {
        this.commentDate = commentDate;
    }

    public int getCommentId() {
        return commentId;
    }

    public int getProductId() {
        return productId;
    }

    public String getComment() {
        return comment;
    }

    public int getRating() {
        return rating;
    }

    public String getCommentDate() {
        return commentDate;
    }
    
}

