
package model;

/**
 *
 * @author ASHOK
 */
public class ShoppingCart {

    
    private int cartId;
    private int pid;
    private int qty;
    private double price;
    private int ucid;
    
    
    public ShoppingCart() {
    }
    
    public ShoppingCart(int cartId, int pid, int qty, double price) {
        this.cartId = cartId;
        this.pid = pid;
        this.qty = qty;
        this.price = price;
    }
     
    public ShoppingCart(int pid, int qty, double price, int ucid) {
        this.pid = pid;
        this.qty = qty;
        this.price = price;
        this.ucid = ucid;
    }

    public void setCartId(int cartId) {
        this.cartId = cartId;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setUcid(int ucid) {
        this.ucid = ucid;
    }

    public int getCartId() {
        return cartId;
    }

    public int getPid() {
        return pid;
    }

    public int getQty() {
        return qty;
    }

    public double getPrice() {
        return price;
    }

    public int getUcid() {
        return ucid;
    }
    
}
