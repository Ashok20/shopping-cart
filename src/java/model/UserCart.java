
package model;

/**
 *
 * @author ASHOK
 */
public class UserCart {

    private int ucid;
    private int uid;
    
    public UserCart() {
    }

    public UserCart(int ucid, int uid) {
        this.ucid = ucid;
        this.uid = uid;
    }

    public void setUcid(int ucid) {
        this.ucid = ucid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getUcid() {
        return ucid;
    }

    public int getUid() {
        return uid;
    }
    
}
