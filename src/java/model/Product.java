package model;

public class Product {
    
    private int id;
    private String name;
    private String detail;
    private double cost;
    private double price;
    private String type;
    private String path;
    
    //No Argument Constructor
    public Product() {
    }
    
    //Constructor
    public Product(int id, String name, String detail, double cost, double price, String type, String path) {
        this.id = id;
        this.name = name;
        this.detail = detail;
        this.cost = cost;
        this.price = price;
        this.type = type;
        this.path = path;
    }

    //Constructor
    public Product(int id, String name, String detail, double cost, double price) {
        this.id = id;
        this.name = name;
        this.detail = detail;
        this.cost = cost;
        this.price = price;
    }
    
    
    //Constructor
    public Product(String name, String detail, double cost, double price, String type, String path) {
        this.name = name;
        this.detail = detail;
        this.cost = cost;
        this.price = price;
        this.type = type;
        this.path = path;
    }
    
    
    //Constructor
    public Product(String name, String detail, double cost, double price, String type) {
        this.name = name;
        this.detail = detail;
        this.cost = cost;
        this.price = price;
        this.type = type;
    }
    
    
    //Setter
    public void setId(int id) {
        this.id = id;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setDetail(String detail) {
        this.detail = detail;
    }
    
    public void setCost(double cost) {
        this.cost = cost;
    }
    
    public void setPrice(double price) {
        this.price = price;
    }
    
    public void setType(String type) {
        this.type = type;
    }
            
    public void setPath(String path) {
        this.path = path;
    }
    
    
    //Getter
    public int getId() {
        return id;
    }
    
    public String getName() {
        return name;
    }
    
    public String getDetail() {
        return detail;
    }
    
    public double getCost() {
        return cost;
    }
    
    public double getPrice() {
        return price;
    }
    
    public String getType() {
        return type;
    }
    
    public String getPath() {
        return path;
    }
          
}
