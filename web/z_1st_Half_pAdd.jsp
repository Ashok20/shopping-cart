<jsp:include page="headerInclude.jsp" > 
    <jsp:param name="title" value="Add Item" />
    <jsp:param name="css2" value="css/editProductStyle.css" />
</jsp:include>
    

<div class="row">
    <div class="col-sm-4 col-md-4 col-sm-offset-4 col-md-offset-4">
        <div class="thumbnail">
            <img id="target" src="duplicateImage/" alt="your image">
            <div class="form-group normalFontSize form-inline">
                <label class="col-sm-2 control-label">Image</label>
                <input type="file" id="src" name="imagefile" value="">
            </div>
        </div>            
    </div>
    </div>

    <div class="row">
        <div class="col-sm-6 col-md-6 col-sm-offset-3 col-md-offset-3">
            <div class="thumbnail">
                <div class="caption">
                    <h3><b>Add New Product</b></h3>
                    <br />
                    <form action="AddProductServlet" method="GET">
                        <div class="form-group normalFontSize form-inline">
                            <label class="col-sm-2 control-label">Name</label>
                            <input type="text" name="pname" class="form-control" id="exampleInputEmail1" value="">
                        </div>
                        <div class="form-group normalFontSize form-inline">
                            <label class="col-sm-2 control-label">Details</label>
                            <textarea class="form-control heightWidthTextarea" name="pdetail"></textarea>
                        </div>
                        <div class="form-group normalFontSize form-inline">
                            <label class="col-sm-2 control-label">Cost</label>                                                
                            <div class="input-group">
                                <div class="input-group-addon">$</div>
                                <input type="text" name="pcost" class="form-control" id="exampleInputPassword1" value="">
                                <div class="input-group-addon">.00</div>
                            </div>                    
                        </div>
                        <div class="form-group normalFontSize form-inline">
                            <label class="col-sm-2 control-label">Price</label>                        
                            <div class="input-group">
                                <div class="input-group-addon">$</div>
                                <input type="text" name="pprice" class="form-control" id="exampleInputPassword1" value="">
                                <div class="input-group-addon">.00</div>                                
                            </div>                       
                        </div>
                        <div class="form-group normalFontSize form-inline">
                            <label class="col-sm-2 control-label" for="exampleInputCategory">Category</label>
                            <select name="ptype" value="" >
                                <option>MEN</option>
                                <option>WOMEN</option>
                                <option>BOYS</option>
                                <option>GIRLS</option>
                            </select>
                        </div>
                        <br />                        
                        <p>                    
                            <input type="submit" class="btn btn-primary" value="Save"/>
                            <a href='index.jsp' class="btn btn-danger" onclick="return confirm('Are you sure you want to Cancel?')" >Cancel</a>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>

                                                  
<script>
                                                       
    function showImage(src,target) {
      var fr=new FileReader();
      // when image is loaded, set the src of the image where you want to display it
      fr.onload = function(e) { target.src = this.result; };
      src.addEventListener("change",function() {
        // fill fr with image data    
        fr.readAsDataURL(src.files[0]);
      });
    }

    var src = document.getElementById("src");
    var target = document.getElementById("target");
    showImage(src,target);
                         
</script>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
<jsp:include page="footerInclude.jsp" />