<jsp:include page="headerInclude.jsp" > 
    <jsp:param name="title" value="MEN" />
    <jsp:param name="css2" value="css/newItem.css" />
</jsp:include>
    
    
<form action="ImgProcessServlet" method="POST" enctype="multipart/form-data">
    <div class="row">
        <div class="col-sm-4 col-md-4 col-sm-offset-4 col-md-offset-4">
            <h2>Attach Product Image</h2>
            <div id="addproduct" class="panel panel-default">
                <div class="thumbnail">
                    <img id="target" src="duplicateImage/" alt="your image">
                    <div class="form-group normalFontSize form-inline">
                        <label class="col-sm-2 control-label">Image</label>
                        <input type="file" id="src" name="imagefile" value="">
                    </div>
                </div>            
            </div>
            <input type="submit" value="Upload" class="btn btn-default" />
        </div>
    </div>
</form>

        
<script>
                                                       
    function showImage(src,target) {
      var fr=new FileReader();
      // when image is loaded, set the src of the image where you want to display it
      fr.onload = function(e) { target.src = this.result; };
      src.addEventListener("change",function() {
        // fill fr with image data    
        fr.readAsDataURL(src.files[0]);
      });
    }

    var src = document.getElementById("src");
    var target = document.getElementById("target");
    showImage(src,target);
                         
</script>
    
<jsp:include page="footerInclude.jsp" />