package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class z_005f2nd_005fHalf_005fpAdd_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "headerInclude.jsp" + "?" + org.apache.jasper.runtime.JspRuntimeLibrary.URLEncode("title", request.getCharacterEncoding())+ "=" + org.apache.jasper.runtime.JspRuntimeLibrary.URLEncode("MEN", request.getCharacterEncoding()) + "&" + org.apache.jasper.runtime.JspRuntimeLibrary.URLEncode("css2", request.getCharacterEncoding())+ "=" + org.apache.jasper.runtime.JspRuntimeLibrary.URLEncode("css/newItem.css", request.getCharacterEncoding()), out, false);
      out.write("\n");
      out.write("    \n");
      out.write("\n");
      out.write("<div class=\"row\">\n");
      out.write("    <div class=\"col-md-offset-4 col-xs-offset-5 col-xs-2 col-md-4\">\n");
      out.write("        \n");
      out.write("        <div class=\"alert alert-success\" role=\"alert\">           \n");
      out.write("            <h5>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope[\"message\"]}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</h5>\n");
      out.write("        </div>\n");
      out.write("        <div class=\"alert alert-success\" role=\"alert\">\n");
      out.write("            <h5>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope[\"status\"]}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</h5>            \n");
      out.write("        </div>\n");
      out.write("    </div>\n");
      out.write("</div>\n");
      out.write("    \n");
      out.write("<div class=\"row\">\n");
      out.write("    <div class=\"col-md-offset-5 col-xs-offset-5 col-xs-2 col-md-2\">       \n");
      out.write("        <div class=\"thumbnail\">              \n");
      out.write("            <a href=\"women.jsp\"><img src=\"image/1.jpg\" alt=\"\"/></a>           \n");
      out.write("            <div class=\"caption\">\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("    </div>\n");
      out.write("</div>\n");
      out.write("    \n");
      out.write("<div class=\"row\">\n");
      out.write("    <div class=\"col-md-offset-3 col-xs-offset-2 col-xs-8 col-md-6\">       \n");
      out.write("        <div id=\"addproduct\" class=\"panel panel-default\">\n");
      out.write("            <div class=\"panel-body\">\n");
      out.write("                \n");
      out.write("                <form action=\"ImgProcessServlet\" method=\"POST\" enctype=\"multipart/form-data\">\n");
      out.write("                    <div class=\"form-group\">\n");
      out.write("                        <label for=\"exampleInputFile\">Attach Product Image</label>\n");
      out.write("                        <input type=\"file\" name=\"imagefile\" class=\"btn\" id=\"exampleInputFile\" value=\"\"/>\n");
      out.write("                        <label for=\"exampleInputFile\"></label>\n");
      out.write("                            \n");
      out.write("                    </div>                    \n");
      out.write("                    <input type=\"submit\" value=\"Upload\" class=\"btn btn-default\" />\n");
      out.write("                </form>\n");
      out.write("                    \n");
      out.write("            </div>\n");
      out.write("        </div>            \n");
      out.write("    </div>\n");
      out.write("</div>\n");
      out.write("    \n");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "footerInclude.jsp", out, false);
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
