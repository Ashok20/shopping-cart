package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import model.Product;
import java.sql.SQLException;
import control.ControlProduct;
import java.util.ArrayList;

public final class girls_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "headerInclude.jsp" + "?" + org.apache.jasper.runtime.JspRuntimeLibrary.URLEncode("title", request.getCharacterEncoding())+ "=" + org.apache.jasper.runtime.JspRuntimeLibrary.URLEncode("GIRLS", request.getCharacterEncoding()) + "&" + org.apache.jasper.runtime.JspRuntimeLibrary.URLEncode("css2", request.getCharacterEncoding())+ "=" + org.apache.jasper.runtime.JspRuntimeLibrary.URLEncode("#", request.getCharacterEncoding()), out, false);
      out.write("\n");
      out.write("\n");
      out.write("\n");

    String sql = "SELECT * FROM product WHERE ptype='GIRLS'";
    ArrayList<Product> items = control.ControlProduct.getMultiProducts(sql);   

      out.write("\n");
      out.write(" \n");
      out.write("\n");
      out.write("\n");
      out.write("<div class=\"row\">\n");
      out.write("    <div class=\"col-md-1\"></div>\n");
      out.write("    <div class=\"col-md-10\">\n");
      out.write("                \n");
      out.write("        ");
  
            while(!items.isEmpty()){
                out.println("<div class='col-md-3 col-sm-6'>");
                out.println("<div class='thumbnail'>");              
                out.println("<a href='viewProduct.jsp?productId="+String.valueOf(items.get(0).getId())+"'><img src='duplicateImage/"+items.get(0).getPath()+"' alt=''/></a>");           
                out.println("<div class='caption'>");
                out.println("<h3>"+items.get(0).getName()+"</h3>");
                out.println("<p><h5>USD "+items.get(0).getPrice()+"0</h5></p>");
                out.println("<p><a href='viewProduct.jsp?productId="+String.valueOf(items.get(0).getId())+"' class='btn btn-primary' role='button'>View Item</a></p>");
                out.println("</div></div></div>"); 
                items.remove(0);
            }

        
      out.write("\n");
      out.write("                        \n");
      out.write("    </div>\n");
      out.write("    <div class=\"col-md-1\"></div>\n");
      out.write("</div>\n");
      out.write("    \n");
      out.write("    \n");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "footerInclude.jsp", out, false);
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
