package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class z_005f1st_005fHalf_005fpAdd_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "headerInclude.jsp" + "?" + org.apache.jasper.runtime.JspRuntimeLibrary.URLEncode("title", request.getCharacterEncoding())+ "=" + org.apache.jasper.runtime.JspRuntimeLibrary.URLEncode("Add Item", request.getCharacterEncoding()) + "&" + org.apache.jasper.runtime.JspRuntimeLibrary.URLEncode("css2", request.getCharacterEncoding())+ "=" + org.apache.jasper.runtime.JspRuntimeLibrary.URLEncode("css/editProductStyle.css", request.getCharacterEncoding()), out, false);
      out.write("\n");
      out.write("    \n");
      out.write("\n");
      out.write("<div class=\"row\">\n");
      out.write("    <div class=\"col-sm-4 col-md-4 col-sm-offset-4 col-md-offset-4\">\n");
      out.write("        <div class=\"thumbnail\">\n");
      out.write("            <img id=\"target\" src=\"duplicateImage/\" alt=\"your image\">\n");
      out.write("            <div class=\"form-group normalFontSize form-inline\">\n");
      out.write("                <label class=\"col-sm-2 control-label\">Image</label>\n");
      out.write("                <input type=\"file\" id=\"src\" name=\"imagefile\" value=\"\">\n");
      out.write("            </div>\n");
      out.write("        </div>            \n");
      out.write("    </div>\n");
      out.write("    </div>\n");
      out.write("\n");
      out.write("    <div class=\"row\">\n");
      out.write("        <div class=\"col-sm-6 col-md-6 col-sm-offset-3 col-md-offset-3\">\n");
      out.write("            <div class=\"thumbnail\">\n");
      out.write("                <div class=\"caption\">\n");
      out.write("                    <h3><b>Add New Product</b></h3>\n");
      out.write("                    <br />\n");
      out.write("                    <form action=\"AddProductServlet\" method=\"GET\">\n");
      out.write("                        <div class=\"form-group normalFontSize form-inline\">\n");
      out.write("                            <label class=\"col-sm-2 control-label\">Name</label>\n");
      out.write("                            <input type=\"text\" name=\"pname\" class=\"form-control\" id=\"exampleInputEmail1\" value=\"\">\n");
      out.write("                        </div>\n");
      out.write("                        <div class=\"form-group normalFontSize form-inline\">\n");
      out.write("                            <label class=\"col-sm-2 control-label\">Details</label>\n");
      out.write("                            <textarea class=\"form-control heightWidthTextarea\" name=\"pdetail\"></textarea>\n");
      out.write("                        </div>\n");
      out.write("                        <div class=\"form-group normalFontSize form-inline\">\n");
      out.write("                            <label class=\"col-sm-2 control-label\">Cost</label>                                                \n");
      out.write("                            <div class=\"input-group\">\n");
      out.write("                                <div class=\"input-group-addon\">$</div>\n");
      out.write("                                <input type=\"text\" name=\"pcost\" class=\"form-control\" id=\"exampleInputPassword1\" value=\"\">\n");
      out.write("                                <div class=\"input-group-addon\">.00</div>\n");
      out.write("                            </div>                    \n");
      out.write("                        </div>\n");
      out.write("                        <div class=\"form-group normalFontSize form-inline\">\n");
      out.write("                            <label class=\"col-sm-2 control-label\">Price</label>                        \n");
      out.write("                            <div class=\"input-group\">\n");
      out.write("                                <div class=\"input-group-addon\">$</div>\n");
      out.write("                                <input type=\"text\" name=\"pprice\" class=\"form-control\" id=\"exampleInputPassword1\" value=\"\">\n");
      out.write("                                <div class=\"input-group-addon\">.00</div>                                \n");
      out.write("                            </div>                       \n");
      out.write("                        </div>\n");
      out.write("                        <div class=\"form-group normalFontSize form-inline\">\n");
      out.write("                            <label class=\"col-sm-2 control-label\" for=\"exampleInputCategory\">Category</label>\n");
      out.write("                            <select name=\"ptype\" value=\"\" >\n");
      out.write("                                <option>MEN</option>\n");
      out.write("                                <option>WOMEN</option>\n");
      out.write("                                <option>BOYS</option>\n");
      out.write("                                <option>GIRLS</option>\n");
      out.write("                            </select>\n");
      out.write("                        </div>\n");
      out.write("                        <br />                        \n");
      out.write("                        <p>                    \n");
      out.write("                            <input type=\"submit\" class=\"btn btn-primary\" value=\"Save\"/>\n");
      out.write("                            <a href='index.jsp' class=\"btn btn-danger\" onclick=\"return confirm('Are you sure you want to Cancel?')\" >Cancel</a>\n");
      out.write("                        </p>\n");
      out.write("                    </form>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("    </div>\n");
      out.write("\n");
      out.write("                                                  \n");
      out.write("<script>\n");
      out.write("                                                       \n");
      out.write("    function showImage(src,target) {\n");
      out.write("      var fr=new FileReader();\n");
      out.write("      // when image is loaded, set the src of the image where you want to display it\n");
      out.write("      fr.onload = function(e) { target.src = this.result; };\n");
      out.write("      src.addEventListener(\"change\",function() {\n");
      out.write("        // fill fr with image data    \n");
      out.write("        fr.readAsDataURL(src.files[0]);\n");
      out.write("      });\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    var src = document.getElementById(\"src\");\n");
      out.write("    var target = document.getElementById(\"target\");\n");
      out.write("    showImage(src,target);\n");
      out.write("                         \n");
      out.write("</script>\n");
      out.write("    \n");
      out.write("    \n");
      out.write("    \n");
      out.write("    \n");
      out.write("    \n");
      out.write("    \n");
      out.write("    \n");
      out.write("    \n");
      out.write("    \n");
      out.write("    \n");
      out.write("    \n");
      out.write("    \n");
      out.write("    \n");
      out.write("    \n");
      out.write("    \n");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "footerInclude.jsp", out, false);
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
