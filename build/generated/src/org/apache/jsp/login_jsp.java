package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class login_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <link rel=\"icon\" type=\"image/x-icon\" href=\"image/titleLogo1.ico\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/login.css\" type=\"text/css\">\n");
      out.write("\n");
      out.write("        <title>Login Page</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <div class=\"container\">\n");
      out.write("            <div class=\"info\">\n");
      out.write("                <h1>Login Form</h1>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        <div class=\"form\">\n");
      out.write("            <div class=\"thumbnail\"><img src=\"image/shopping-cart.svg\"/></div>\n");
      out.write("            <form action=\"RegisterNewUser\" method=\"POST\" class=\"register-form\">\n");
      out.write("                <input type=\"text\" name=\"userName\" placeholder=\"username\"/>\n");
      out.write("                <input type=\"text\" name=\"fullName\" placeholder=\"full name\"/>\n");
      out.write("                <input type=\"password\" name=\"pass\" placeholder=\"password\"/>\n");
      out.write("                <input type=\"text\" name=\"email\" placeholder=\"email address\"/>\n");
      out.write("                <input class=\"btnUserAuthentication\" type=\"submit\" value=\"create\" />\n");
      out.write("                <p class=\"message\">Already registered? <a href=\"#\">Sign In</a></p>\n");
      out.write("            </form>\n");
      out.write("            <form action=\"UserAuthentication\" method=\"POST\" class=\"login-form\">\n");
      out.write("                <input type=\"text\" name=\"userName\" value=\"\" placeholder=\"username\"/>\n");
      out.write("                <input type=\"password\" name=\"password\" value=\"\" placeholder=\"password\"/>\n");
      out.write("                <input class=\"btnUserAuthentication\" type=\"submit\" value=\"login\" />\n");
      out.write("                <p class=\"message\">Not registered? <a href=\"#\">Create an account</a></p>\n");
      out.write("            </form>\n");
      out.write("            <br />\n");
      out.write("            <label id=\"loginError\">");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${param.message}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</label>\n");
      out.write("        </div>\n");
      out.write("        <script src=\"js/jquery-1.9.1.min.js\" type=\"text/javascript\"></script>\n");
      out.write("\n");
      out.write("        <script>\n");
      out.write("            $('.message a').click(function () {\n");
      out.write("                $('form').animate({height: \"toggle\", opacity: \"toggle\"}, \"slow\");\n");
      out.write("            });\n");
      out.write("        </script>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
