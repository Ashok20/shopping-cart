<%@page import="java.util.ArrayList"%>
<%@page import="model.Product"%>
<jsp:include page="headerInclude.jsp" > 
    <jsp:param name="title" value="WOMEN" />
    <jsp:param name="css2" value="#" />
</jsp:include>



<%
    String sql = "SELECT * FROM product WHERE ptype='WOMEN'";
    ArrayList<Product> items = control.ControlProduct.getMultiProducts(sql);   
%>
 

<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
                
        <%  
            while(!items.isEmpty()){
                out.println("<div class='col-md-3 col-sm-6'>");
                out.println("<div class='thumbnail'>");              
                out.println("<a href='viewProduct.jsp?productId="+String.valueOf(items.get(0).getId())+"'><img src='duplicateImage/"+items.get(0).getPath()+"' alt=''/></a>");           
                out.println("<div class='caption'>");
                out.println("<h3>"+items.get(0).getName()+"</h3>");
                out.println("<p><h5>USD "+items.get(0).getPrice()+"0</h5></p>");
                out.println("<p><a href='viewProduct.jsp?productId="+String.valueOf(items.get(0).getId())+"' class='btn btn-primary' role='button'>View Item</a></p>");
                out.println("</div></div></div>"); 
                items.remove(0);
            }

        %>
                        
    </div>
    <div class="col-md-1"></div>
</div>



<jsp:include page="footerInclude.jsp" />