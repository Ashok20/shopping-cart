    
<%@page import="java.util.ArrayList"%>
<%@page import="model.Comment"%>
<%@page import="control.ControlComment"%>
<%@page import="model.Product"%>
<%@page import="control.ControlProduct"%>

<jsp:include page="headerInclude.jsp" > 
    <jsp:param name="title" value="Edit Product" />
    <jsp:param name="css2" value="css/editProductStyle.css" />
</jsp:include>


<%
    Product productObj = ControlProduct.getProduct(Integer.parseInt(request.getParameter("productId")));
%>


    <div class="row">
        <div class="col-sm-4 col-md-4 col-sm-offset-4 col-md-offset-4">
            <div class="thumbnail">
                <img id="target" src="duplicateImage/<%=productObj.getPath()%>" alt="your image">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6 col-md-6 col-sm-offset-3 col-md-offset-3">
            <div class="thumbnail">
                <div class="caption">
                    <h3><b>Product Details</b></h3>
                    <br />
                    <form action="EditProductProcess" method="GET" >
                        <div class="form-group normalFontSize form-inline">
                            <label class="col-sm-2 control-label">ID</label>
                            <label class="control-label"><%=productObj.getId()%></label>                    
                        </div>
                        <div class="form-group normalFontSize form-inline">
                            <label class="col-sm-2 control-label">Name</label>
                            <input type="text" name="productName" class="form-control" id="exampleInputEmail1" value="<%=productObj.getName()%>">
                        </div>
                        <div class="form-group normalFontSize form-inline">
                            <label class="col-sm-2 control-label">Details</label>
                            <textarea class="form-control heightWidthTextarea" name="productDetail"><%=productObj.getDetail()%></textarea>
                        </div>
                        <div class="form-group normalFontSize form-inline">
                            <label class="col-sm-2 control-label">Cost</label>                                                
                            <div class="input-group">
                                <div class="input-group-addon">$</div>
                                <input type="text" name="productCost" class="form-control" id="exampleInputPassword1" value="<%=productObj.getCost()%>">
                                <div class="input-group-addon">.00</div>
                            </div>                    
                        </div>
                        <div class="form-group normalFontSize form-inline">
                            <label class="col-sm-2 control-label">Price</label>                        
                            <div class="input-group">
                                <div class="input-group-addon">$</div>
                                <input type="text" name="productPrice" class="form-control" id="exampleInputPassword1" value="<%=productObj.getPrice()%>">
                                <div class="input-group-addon">.00</div>
                                <input style="display: none" type="text" name="productId" value="<%=productObj.getId() %>" />
                            </div>                       
                        </div>                        
                        <br />                        
                        <p>                    
                            <input type="submit" class="btn btn-primary" value="Update"/>
                            <a href='DeleteProductProcess?click=yes&productId=<%=productObj.getId()%>' class="btn btn-danger" onclick="return confirm('Are you sure you want to Delete this Item ?')" >Delete</a>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>

                                                  
<script>
                                                       
    function showImage(src,target) {
      var fr=new FileReader();
      // when image is loaded, set the src of the image where you want to display it
      fr.onload = function(e) { target.src = this.result; };
      src.addEventListener("change",function() {
        // fill fr with image data    
        fr.readAsDataURL(src.files[0]);
      });
    }

    var src = document.getElementById("src");
    var target = document.getElementById("target");
    showImage(src,target);
                         
</script>
                    
<jsp:include page="footerInclude.jsp" />

