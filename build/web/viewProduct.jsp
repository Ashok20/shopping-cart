    
<%@page import="java.util.ArrayList"%>
<%@page import="model.Comment"%>
<%@page import="control.ControlComment"%>
<%@page import="model.Product"%>
<%@page import="control.ControlProduct"%>

<jsp:include page="headerInclude.jsp" > 
    <jsp:param name="title" value="Product Details" />
    <jsp:param name="css2" value="css/viewProductStyle.css" />
</jsp:include>


<%
    Product productObj = ControlProduct.getProduct(Integer.parseInt(request.getParameter("productId")));
    ArrayList<Comment> listObj = ControlComment.getMultiComments(productObj.getId());
%>
    
<div class="row">
    
    <div class="col-md-6 col-md-offset-3">
        
        <div class="product-view">            
                <div class="product-header">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="product-image"><img src="duplicateImage/<%=productObj.getPath() %>" alt=""/></div>
                        </div>
                        <div class="col-md-4">
                            <div class="product-caption">
                                <h2><%=productObj.getName() %></h2>
                                <div class="price">
                                    $ <%=productObj.getPrice() %>
                                </div>
                                <div class="special-info">
                                    <ul class="special">
                                        <li>Free shipping</li>
                                        <li>Advanced stylingt</li>
                                        <li>Better comfort</li>
                                    </ul>
                                </div>
                                <form action="AddToCartProcess" class="btnInlineBlock">
                                    <input style="display: none" type="text" name="txtProductId" value="<%=productObj.getId() %>" />                                
                                    <p><input class="btn btn-primary btn-sm" type="submit" value="Add to Cart" /></p>
                                </form>
                                
                                <%
                                    if(session.getAttribute("userType") != null && session.getAttribute("userType").toString().equals("ADMIN")){
                                        out.print("<form class='btnInlineBlock'>");
                                        out.print("<p><a href='editProduct.jsp?productId="+productObj.getId()+"' class='btn btn-success btn-sm'>Edit Product</a></p>");
                                        out.print("</form>");
                                    }
                                %>
                                                      
                                
                            </div>
                        </div>
                    </div>
                </div>
            
                
                <div class="product-desc">
                
                <div class="desc-item">
                    <div class="desc-title clearfix">
                        <div class="desc-title-bg">Product Description</div>
                    </div>
                    <div class="desc-text">
                        <p><%=productObj.getDetail() %> </p>
                    </div>
                </div>
                <div class="desc-item">
                    <div class="desc-title clearfix">
                        <div class="desc-title-bg">Customer Reviews</div>
                    </div>
                    <div class="desc-text">
                        
                        
                        
                        <%  
                            while(!listObj.isEmpty()){

                                out.println("<div class='review'>");
                                out.println("<blockquote>");
                                out.println("<p>"+listObj.get(0).getComment()+"</p>");
                                out.println("<div class='rating'>");
                                out.println("<strong><span class='review-footer'>Rating :</span></strong> ");

                                for(int i = listObj.get(0).getRating(); i>0; i--){
                                    out.println("<span>&#9733;</span>");
                                }

                                for(int i = 5-(listObj.get(0).getRating()); i>0; i--){
                                    out.println("<span>&#9734;</span>");
                                }

                                out.println("</div>");
                                out.println("<p class='review-footer'><strong>Anonymous customer @ "+listObj.get(0).getCommentDate()+"</strong></p>");
                                out.println("</blockquote>");
                                out.println("</div>");

                                listObj.remove(0);
                            }

                        %>
                        
                    </div>
                </div>
                    
                <div class="desc-item">
                    <div class="desc-title clearfix">
                        <div class="desc-title-bg">Write a review</div>
                    </div>
                    <div class="desc-text">
                        <form action="CommentProcess" method="GET">
                            <div class="form-group">
                                <label>Your review</label>
                                <textarea class="form-control" name="commentText" placeholder="Write your review here..."></textarea>
                            </div>
                            <div class="form-group">
                                <label>Ratings : </label>
                                <select name="rating" value="" style="color: #e1b13c;">
                                    <option value="1"><span>&#9733;</span></option>
                                    <option value="2"><span>&#9733;</span><span>&#9733;</span></option>
                                    <option value="3"><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span></option>
                                    <option value="4"><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span></option>
                                    <option value="5"><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span></option>
                                </select>
                            </div>
                            <input style="display: none" type="text" name="txtProductId" value="<%=productObj.getId() %>" />
                            <br />
                            <input type="submit" value="Submit" class="btn btn-primary" />
                        </form>
                    </div>
                </div>                     
            </div>                      
        </div>                  
    </div>       
</div>
    
    
<jsp:include page="footerInclude.jsp" />