    
<jsp:include page="headerInclude.jsp" > 
    <jsp:param name="title" value="Home" />
    <jsp:param name="css2" value="#" />
</jsp:include>

    <div class="content">
    
    <div class="content__outer">
            <div class="content__inner">
            
            <div class="content__sliderWrapper">
                <div class="content__slider">
                    <div class="content__sliderItemOuter">
                        <div class="content__sliderItem">
                            <div class="content__sliderTitle">
                                
                                Nike Air Max 2015
                            </div>
                            <div class="content__sliderContent">
                                <div class="content__sliderContentImg">
                                    <img src="image/mainShoe.png" alt=""/>                                         
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>                   
            
    </div>
</div>


    
<jsp:include page="footerInclude.jsp" />