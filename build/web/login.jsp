
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" type="image/x-icon" href="image/titleLogo1.ico">
        <link rel="stylesheet" href="css/login.css" type="text/css">

        <title>Login Page</title>
    </head>
    <body>
        <div class="container">
            <div class="info">
                <h1>Login Form</h1>
            </div>
        </div>
        <div class="form">
            <div class="thumbnail"><img src="image/shopping-cart.svg"/></div>
            <form action="RegisterNewUser" method="POST" class="register-form">
                <input type="text" name="userName" placeholder="username"/>
                <input type="text" name="fullName" placeholder="full name"/>
                <input type="password" name="pass" placeholder="password"/>
                <input type="text" name="email" placeholder="email address"/>
                <input class="btnUserAuthentication" type="submit" value="create" />
                <p class="message">Already registered? <a href="#">Sign In</a></p>
            </form>
            <form action="UserAuthentication" method="POST" class="login-form">
                <input type="text" name="userName" value="" placeholder="username"/>
                <input type="password" name="password" value="" placeholder="password"/>
                <input class="btnUserAuthentication" type="submit" value="login" />
                <p class="message">Not registered? <a href="#">Create an account</a></p>
            </form>
            <br />
            <label id="loginError">${param.message}</label>
        </div>
        <script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>

        <script>
            $('.message a').click(function () {
                $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
            });
        </script>
    </body>
</html>
