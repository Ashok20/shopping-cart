    
<%@page import="model.ShoppingCart"%>
<%@page import="java.util.ArrayList"%>
<%@page import="control.ControlUserCart"%>
<%@page import="model.UserCart"%>
<%@page import="control.ControlProduct"%>
<%@page import="model.Product"%>

<jsp:include page="headerInclude.jsp" > 
    <jsp:param name="title" value="My Cart" />
    <jsp:param name="css2" value="css/cartStyle.css" />
</jsp:include>
    
 

<%
    //int userId = (Integer)session.getAttribute("userId");
    HttpSession ucidSession = request.getSession();
    int ucid = (Integer)ucidSession.getAttribute("ucid");
    
    //UserCart userCartObj = ControlUserCart.getUserCartInfo(userId);
    
    ArrayList<ShoppingCart> items = control.ControlShoppingCart.getShoppingCartProducts(ucid);
%>



<div class="row">
    
    <div class="col-md-6 col-md-offset-3">      
        
        <div class="cart-view">
            <table class="table">
                <thead>
                    <tr>
                        <th>Product
                        </th>
                        <th>No of Pairs</th>
                        <th>Sub Total</th>
                        <th>#</th>
                    </tr>
                </thead>
                <tbody>

        <%  
            while(!items.isEmpty()){

                out.println("<tr>");
                out.println("<td>");
                out.println("<div class='cart-p-desc clearfix'>");
                out.println("<div class='cart-p-thumb'>");
                out.println("<img src='duplicateImage/"+ControlProduct.getProduct(items.get(0).getPid()).getPath()+"' alt=''>");
                out.println("</div>");
                out.println("<div class='cart-p-name'>");
                items.get(0).getPid();
                out.println("</div>");
                out.println("</div>");
                out.println("</td>");
                out.println("<td><div class='cart-qty'><input class='form-control' value='1'></div></td>");
                out.println("<td>$ "+items.get(0).getPrice()+"</td>");
                out.println("<td><a href='DeleterFromCartProcess?productId="+items.get(0).getPid()+"' class='btn btn-default btn-xs' style='color:red'><i class='fa fa-trash' aria-hidden='true'></i></a></td>");
                out.println("</tr>");
                
                items.remove(0);
            }
        %>                          

<!--                    <tr>
                        <td>
                        </td>
                        <td>
                            <strong>Total</strong>
                        </td>
                        <td>
                            <strong>  $ 345</strong>
                        </td>
                        <td>
                        </td>
                    </tr>-->
                </tbody>
            </table>
            <div style="text-align:right;padding:20px;">
                <form action="EndProcess">
                    <input class="btn btn-primary" type="submit" value="Continue checkout" />
                </form>
            </div>
        </div>
    </div>
        
</div>
    
<jsp:include page="footerInclude.jsp" />