-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 20, 2017 at 06:36 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nikedb`
--

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `cid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `comment` text NOT NULL,
  `rating` int(11) NOT NULL,
  `cdate` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`cid`, `pid`, `comment`, `rating`, `cdate`) VALUES
(1, 1, 'Nice Shoe... I love this.', 4, '2017-05-20'),
(2, 1, 'This is an amazing product. I need to buy this as soon as possible. ', 3, '2017-05-10'),
(7, 1, 'Look and feel of this shoe is very nice. I\'m glad i bought this...', 5, '2017-05-20'),
(8, 6, 'Nice Shoes.... ', 4, '2017-05-20'),
(9, 5, 'wow....', 2, '2017-05-20'),
(23, 3, 'In Canada, we don\'t have to take off our shoes (unless boarding a US-bound flight). Why is this thought essential for aircraft security in the US but not in Canada?\r\nAnd the ultimate irony is this: At my local airport, domestic passengers (shoes not x-rayed) and US-bound passengers (shoes have been x-rayed) can intermingle \r\nafter passing through security.', 4, '2016-12-20'),
(26, 4, 'If you are looking for a stylish running shoe that wraps around your foot perfectly while providing the ultimate cushioning, \r\nthere is no other shoe that is better than this. The shelled grooves on the side that flow to the back provides structure as well as style, \r\nwhile the mesh upper feels amazing while being light and comfortable. But the best part is the air max sole. Nothing compares. \r\nSome say the sole is too thick for runners to keep good contact and maintain balance to the ground but I disagree. \r\nThe shoe is adequately lofty from toe to heel, while allow quite good response laterally. I am seriously contemplating buying 3 pairs of this in different colors.', 4, '2016-10-23'),
(27, 1, 'Well, as usual I\'m stuck between sizes. 10.5 us would be a 44.5 EU. All they have is the 44 and 45. \r\nMy guess is that the 44 would be snug but ok, but the 45 would be a little sloppy. Love this review and the website. \r\nKeep up the great work.', 3, '2017-02-05'),
(28, 2, 'I like them how can i get some...', 2, '2016-08-20'),
(29, 2, 'These shoose just recently caught my attention. I was pretty much convinced to get a pair for this Summer. \r\nUnfortunately the Order function on the Exodus website doesn\'t seem to work and I\'ve read something alluding \r\nto the company not being in business any longer. Now I\'m looking for something that fills the void.', 4, '2017-04-16'),
(30, 2, 'I almost feel happy for this product', 4, '2017-01-04'),
(31, 3, 'We are so far from moving on; we are still terrified of the idea of boarding a plane when not everyone took off their shoes at security, it seems. \r\nThe purpose of a terror attack is to, well, inspire terror, and it has succeeded for ten long years. Once you change your position and behavior in \r\nresponse to an attack, you have proved to potential attackers that they have the power to change you - the biggest incentive you could possibly give them.', 3, '2016-11-13'),
(32, 3, 'In Canada, we don\'t have to take off our shoes (unless boarding a US-bound flight). Why is this thought essential for aircraft security in the US but not in Canada?\r\nAnd the ultimate irony is this: At my local airport, domestic passengers (shoes not x-rayed) and US-bound passengers (shoes have been x-rayed) can intermingle \r\nafter passing through security.', 4, '2016-12-20'),
(33, 3, 'Big deal, so we’ll get to wear shoes while being strip searched, sexually assaulted and being robbed by the TSA screener. Here’s a thought, how about we go \r\nthrough barefoot and skip the feel up and cancer boxes so we can keep an eye on our belongings.This agency is a national disgrace. TSA has done little more \r\nthan harass and molest passengers in a ruse intended to portray airport security. Napolitano and Pistole need to be held accountable for these crimes against our \r\ncivil rights and the Constitution.', 2, '2016-08-20'),
(34, 4, 'I don\'t like the idea of taking my shoes off. Outside the US, I only ever took my shoes off once when going through security - and \r\nthat\'s because it was a pair of boots with metal fasteners in the middle of winter - something which I could understand, but still didn\'t like. But still, we\'re removing our shoes and getting treated like criminals because of an incident that happened 10 years ago...', 3, '2017-01-20'),
(35, 4, 'If you are looking for a stylish running shoe that wraps around your foot perfectly while providing the ultimate cushioning, \r\nthere is no other shoe that is better than this. The shelled grooves on the side that flow to the back provides structure as well as style, \r\nwhile the mesh upper feels amazing while being light and comfortable. But the best part is the air max sole. Nothing compares. \r\nSome say the sole is too thick for runners to keep good contact and maintain balance to the ground but I disagree. \r\nThe shoe is adequately lofty from toe to heel, while allow quite good response laterally. I am seriously contemplating buying 3 pairs of this in different colors.', 4, '2016-10-23'),
(36, 4, 'Shoe runs narrow. I\'m hoping it will stretch out. Not that comfortable.', 1, '2016-08-20'),
(37, 5, 'My experience echos others. The shoe is very narrow and hurts. I normally wear a 12 in Nikes but these shoes are very uncomfortable. Unfortunately a swing and \r\na miss on this year\'s model.', 1, '2017-03-19'),
(38, 5, 'I exchanged these shoes 3 times for the same reason. When I my heel hits the ground they squeak! The first two pairs were really bad - both sides squeaked loudly. \r\nThird and final pair - they didnt squeak at first but now only the right side squeaks. Pretty annoying', 2, '2017-01-04'),
(39, 6, 'This is a good running shoe for somebody whos not doing an extraordinary amount of running. The shoe has good support you dont seem to get leg fatigue. \r\nThey are pretty comfortable I havent had any problems with chafing or blistering. Overall this is a really good shoe for the average Runner.', 4, '2016-12-11'),
(40, 6, 'My ankle really feels uncomfortable because it\'s tight around that area. That feeling goes stronger when you sit down. The rear of the shoe will go against your ankle.', 2, '2017-02-03'),
(41, 6, 'I love the concept ! Love the look - initially they are extremely comfortable , after wearing them for more then 5 minutes I noticed my toes going numb , \r\nthen eventually my feet . Went for a 2 mile run to try them out having the exact same result . Ive never had any shoe do that before . \r\nI wanted them for marathon training , sad to say I had to return them', 2, '2016-12-29'),
(42, 7, 'Shoe runs narrow. I\'m hoping it will stretch out. Not that comfortable.', 2, '2017-02-11'),
(43, 7, 'Just received mine today and was surprised by the poor quality of these premiums. The jumpman print on the insole is crooked, so is the lace lock. The insole is dented. One of the shoe laces is stained...', 3, '2017-01-05'),
(44, 7, 'All the PRM RETRO Releases get it LIT!! The quality is great and I look at it like an investment where you cant lose. I try to Cop a pair of each release and keep them DS so these killer releases will always resurface on the resale market for years to come. Keep on dropping the Heat so we all can feed our Hungry Sneakerhead Addictions.', 3, '2017-02-11'),
(45, 8, 'I love my shoe so much, use it everyday for working and walking. It has a little narrow, but I still love it.', 4, '2016-10-11'),
(46, 8, 'Buyer beware, at about mile 3 these shoes totally lose that snug fit you get when you first put them on. \r\nI think it\'s because the bungee laces give too much and breathability is an issue as well. However, \r\nI would definitely wear these out because of the design and comfort(for walking).', 4, '2016-11-28'),
(47, 8, 'Awesome shoe but extremely disappointed as after wearing them twice something happened with the midsole and it makes a noise every time I take a step. \r\nThe first pair of these I bought I never had a problem with and now I wonder what happened with these', 4, '2016-12-07'),
(48, 8, 'Ive ordered this product for my younger brother as a souvenir. I think its really stylish and beautiful. Plus Nikes products are comfortable and durable.', 4, '2016-12-17'),
(49, 9, 'I started sporting white clothing and these were the best choice for shoes. Very comfortable and breathable, and although white, easy to clean and maintain.', 3, '2016-12-30'),
(50, 9, 'I have add 3 pair of these shoe\'s with no problem\'s but the last pair on the left inside of the shoe has worn away. I\'ve seen similar opinion\'s on other review\'s. Thank \'s', 4, '2016-10-11'),
(51, 9, 'I love the feel of this shoe! At first, I didn\'t think much of the shoe, but I loved the style of the shoe. Comfort is a huge plus!! 5/5', 5, '2016-11-29'),
(52, 10, 'The sneakers started to make squeaking sounds after a couple of weeks of using them. I use them mostly for the gym \r\nbut they do not seem to be very durable after minimal use. Very disappointing', 3, '2017-02-06'),
(53, 10, 'Best shoes i ever had! Best all around shoes in town! Keep up with good work', 4, '2017-01-11'),
(54, 10, 'For those of you like me with flat feet and who over-pronate, avoid this shoe. It\'s listed as a stability shoe but it\'s clearly neutral. It may offer "stability" in terms of being stable for the activities it\'s designed for but it should not be listed under "Stability" when searching the shoes..', 3, '2017-04-11'),
(55, 11, 'This is my first Metcon shoe, and I am not disappointed. It is lightweight, very stable for lifting, and super comfortable with my Elite socks. I would absolutely recommend them. \r\nI have a neutral footfall and these feel the most planted shoes I have worn in a while.', 4, '2017-01-03'),
(56, 11, 'Not an A+ because you need to buy a size bigger. Im a 13 and had to return for a 14. But the shoe it self is superb..', 4, '2016-12-31'),
(57, 11, 'The shoe looks great, Fits greats, and has the support needed for a rigorous training routine. Great grip on the floor. The stability is my main concern and I have no issues with that for being such a light shoe..', 4, '2016-11-17'),
(58, 12, 'better for running than metcons, but little squishy on heavy back squats. Perfect for metcons, just not pure strength sessions. Ideally for anyone who cant run in metcons, but want a stable shoe for workouts.', 4, '2017-01-19'),
(59, 12, 'I have had the 1, 2, and 3 iteration of this shoe. This one fit wise is the best yet. Love the flyknit. I must say though I am disappointed by the laces. I know it has been mentioned before but this is a pretty big oversight in a shoe at this expense.', 4, '2016-10-11'),
(60, 12, 'Love how these feel! Stable while lifting but more comfortable for running and other dynamic movements.', 4, '2016-07-09'),
(61, 13, ' These are hands down the comfiest shoes I have ever worn. Its like walking on a little cloud. \r\nIf you have ever worn huaraches, these have a similar design and feel but the insole is so much more plush. \r\nThey are so cute and so soft, definitely worth the money. I just wish they came in more colors and patterns.', 2, '2016-08-20'),
(62, 13, ' Purchased these for my 14 year old daughter, she absolutely loves the comfort and feel when running. \r\nThe soft cushioned padding makes it a whole lot better. In considering buying a pair for myself.', 4, '2016-09-11'),
(63, 14, 'Comfortable af! Would really recommend these shoes for walking, running, anything casual.', 3, '2016-06-29'),
(64, 14, 'I have multiple pairs prestos and I keep coming back to them. They are extremely comfortable and look good wearing them casually/train/run. All of the above.', 4, '2016-11-02'),
(65, 14, 'I just found out I had heel spurs and Ive been searching everywhere for comfortable shoes to support my feet, as well as comfort the arch in my feet. \r\n& these ones have been such a relief for my feet!!!!', 3, '2017-03-29'),
(66, 15, 'Not gonna lie had to buy my wife two pairs of these shoes.. Great colors!!! \r\nShe said loved them cuz they felt like she was wearing no shoes! great price! Nike is the best hands down!!!!', 3, '2016-12-29'),
(67, 15, 'I primarily use it to train in the gym and for running outdoors and its perfect for both! It offers the \r\nperfect support for my feet, especially the midsole area. Definitely buying another pair.', 4, '2017-05-09'),
(68, 15, 'Tight fight around the ankle but overall good shoe.', 3, '2016-11-12'),
(69, 16, 'These are the most comfortable shoes I have ever worn,like wearing your favorite soft t-shirt. I live in workout clothes and this great print is fun!', 5, '2017-02-12'),
(70, 16, 'Love these. They are super comfortable and stylish. I\'ve received a ton of compliments too. These are my first pair of Nike Juvenate sneakers. \r\nThey are lightweight and I feel like I\'m walking on pillows all-day!', 3, '2016-09-10'),
(71, 17, 'I received them 2 days ago and wore them for a whole day, at work and then for drinks. I was worried about the size but I wear 39 and this fits perfectly. \r\nThey feel very light on my feet and are very comfy. Love them! They go well with everything and good for every casual occasion.', 3, '2017-01-12'),
(72, 17, 'This is pretty shoes and can be wearing daily. However, due to somebody\'s review, I bought one size bigger than usual, but that was so wrong. \r\nI have wide feet, but that one bigger size is too big and long. My suggestion is to buy your own size.', 4, '2017-05-20'),
(73, 17, 'Very nice design, great color, but too small. I\'ve ordered bigger but doesn\'t help. My number for Nike is always 38 but with Cortez even 38.5 is way too small. \r\nI\'ve returned the shoes and reordered bigger unfortunately they don\'t have 39.5. so I have to take 40. Hope will be ok and not too wide..', 3, '2016-11-12'),
(74, 18, 'I had this style shoes back in my high school days...I had several colors to match outfits...I saw them online and I had to have them..', 3, '2016-11-12'),
(75, 18, 'I am usually a size 9.5 in Air Max, but for the Pinnacle\'s I had to order a half size smaller. Could probably go a full size smaller. \r\nOnce the sizing was figured out, super comfortable and look even better in person.', 3, '2016-10-04'),
(76, 18, 'I\'m big fan of nike, and this is my first time to write a review, but it just hurts me. I walked about an hour and hurt on both side so that I couldn\'t walk properly. \r\nIt lasted couple of days. It looks perfect and the leather and color looks great, but I can\'t wear it since, at least, i want to \'walk.', 3, '2016-11-12'),
(77, 19, 'They run really big, Im normally a 7.5 women & I had to order a 6.5, but now theyre prefect', 3, '2016-11-12'),
(78, 19, 'They are very comfortable sneakers and fits just right.', 4, '2016-12-31'),
(79, 19, 'I run about 120 to 130 miles per month and only in your shoes. I love your FLYKNIT collection and LUNAREPIC especially. But this pair is just the best for me. \r\nThank you!', 3, '2017-01-07'),
(80, 20, 'This shoe looks nice on the outside and I tried it in the store as well but after walking for a few hours it felt uncomfortable and it cut my ankle. \r\nI would caution that you check this out in the store to make sure it is the right fit for your foot size.', 2, '2016-08-20'),
(81, 20, 'Im a big fan of the huarache style. These are the comfiest ones yet!', 4, '2016-11-12'),
(82, 20, 'I just got them and they are very lightweight and true to size. They are comfortable on my flat feet and they feel as comfy as bedroom shoes. \r\nI got them on a good sale at the Nike outlet store. The style is so flattering as well. A definite must have.', 4, '2016-07-18'),
(83, 21, 'these shoes are beyond comfy. I love them and get so many compliments on them. Fits amazing and easy to clean. \r\nMy only complaint is I am able to visibly see glue..', 5, '2016-06-07'),
(84, 21, 'I rly don\'t like the color on the bottom I think it\'s okay but I don\'t like it\r\ni LOVE THE SHOES THOUGH', 3, '2016-09-12'),
(85, 21, 'I literally bought these shoes a month ago & they already have holes in them. \r\nIve never been so disappointed in a pair of Nikes. Absolute waste of my money.\r\n', 1, '2016-10-23'),
(86, 22, 'Very comfortable! I love everything about these shoes!', 5, '2016-09-02'),
(87, 22, 'Very comfortable and I love how breathable they are, everyday shoe', 4, '2016-08-13'),
(88, 22, ' These shoes are super cute, light weight, and VERY comfortable. I would recommend them to anyone. \r\nI can\'t wait to wear them for work! I love that I can just slide them on as well! LOVE THEM!\r\n', 4, '2016-11-12'),
(89, 23, 'Love the colour and fit. The normal 8 fit, and I was impressed that I saw high school west this same sneaker (and I\'m 54)', 3, '2017-01-29'),
(90, 23, 'Nice Shoe. Fits true to size. Very comfortable. Soft.', 4, '2017-01-12'),
(91, 23, 'Absolutely love these kicks. i would recommend these to any converse lover, sneakerhead, or anyone who enjoys unique shoes', 3, '2017-02-01'),
(92, 24, 'A bit tight for me foot, but very comfortable. Material is thick, so seems very durable', 2, '2016-08-21'),
(93, 24, 'This is the BEST DESIGN Shoes I\'ve ever had and I\'ve ever seen!!!!! I love this shoes colors & design I\'ve bought 4 of them and will buy more. \r\nI hope Nike didn\'t discontinue this mode.', 5, '2016-07-17'),
(94, 24, 'once again, this model continues to give me excellent traction, flexibility and comfort, while also satisfying my taste for a dark-color shoe. buy it!\r\nwhere can i buy extra variegated laces it comes with?', 4, '2016-11-12'),
(95, 25, 'First time buying Kyrie Nike shoe and my 2 year old loves them. I love that they dont have laces and I can just slip them on with no problem. \r\nMight have to buy different colors.', 3, '2016-08-20'),
(96, 25, 'Very disappointed in these because there are no laces! Very hard to put them on my son!', 4, '2016-08-29'),
(97, 25, 'The elastic laces actually make the shoe a little harder to put on. Sure you don\'t have to tie the shoe \r\nbut you also can\'t loosen them for an easier slide on. I loved the Kyrie 1s and was hoping this style would bring the laces back. \r\nLaces also look a lot more stylish.', 4, '2016-10-02'),
(98, 26, 'I love the shoe but I wish they came in half sizes a 4Y was a little too small but a 5Y was a little to big . I stuck with the 4Y because \r\nthey will stretch over time but until then you can slightly see my toes through the shoe', 2, '2016-10-04'),
(99, 26, 'It is very extreme i like and it fits me it is very comfortable\r\n', 3, '2016-10-17'),
(100, 26, 'Very very comfortable sneaker. Chic and clean lines and great material for durability.', 5, '2016-11-26'),
(101, 27, 'It\'s a great shoe for overall daily wearing and running. I run 5Ks and it\'s great with these. For basketball they\'re fine, \r\nbut they\'re better running shoes than basketball. \r\nI wouldn\'t say it runs THAT small, but only a small length (If 5, then nice to do 5.5, etc.)', 4, '2016-03-03'),
(102, 27, 'It\'s been a month since I\'ve got these and I\'m already in love with these.', 5, '2016-04-18'),
(103, 27, 'I would really recommend this product to people who run. I have this shoe for 2 months now. But when buying this shoe make sure \r\nyou go 2 sizes over to have room to grow into the shoe.', 5, '2016-04-20'),
(104, 28, 'Shoes are light and comfortable but laces are way too short cant keep them tied. Otherwise he loves them', 2, '2016-05-01'),
(105, 28, 'Amazing very comfortable and great durable but the fit is just barely big I recommend', 4, '2016-05-29'),
(106, 28, 'Best shoe on earth-highly recommended-.-Very comfortable,stylish and the perfect price for this shoe make it a legend.\r\n', 5, '2016-07-14'),
(107, 29, 'Its has good stile and plays basketball good. It also mathches well with a lot of things', 4, '2016-04-14'),
(108, 29, 'This is one of my favorite and best jordan sihlouettes. Its very comfortable and durable', 4, '2016-05-15'),
(109, 29, 'I love these shoes! My favorite so far in my collection. They have great fit and very comfortable!\r\n I highly recommend these! Especially the white ones. They last long(the color). \r\nI never have to worry, unless of course theres mud. But other than that they are great shoes!', 5, '2016-06-16'),
(110, 30, 'I have been wearing Air Jordan 1s since I was 11. Little did I know I would still love them 10 years later! Great shoe, \r\ncomfortable, stylish, I\'d buy these again.\r\n', 5, '2016-07-13'),
(111, 30, 'These AF1s are the perfect twist to spruce up your wardrobe. Unique so everyone wont have them. True to size, \r\nbut I have narrow feet and find that they slide off my heels when I walk.', 1, '2016-07-15'),
(112, 30, 'I played middle school basketball for five and a half years and I had the shoes for l year and there still going strong.', 4, '2016-07-24'),
(113, 31, 'It is very Comfortable it fit just how i expected and best of all my team has not lost since i got the shoes and i score 2x The points that i used to score\r\n', 5, '2016-09-02'),
(114, 31, 'The shoes are very comfortable and good looking. And the material is also nice.', 5, '2016-11-11'),
(115, 31, 'Son loved the shoe - colors, fit, suede upper - he couldn\'t be more pleased and that\'s saying a lot for a picky 12 y.o.', 3, '2016-12-25'),
(116, 32, 'Comfortable and nice fit, wide enough for my flat feet', 4, '2016-11-12'),
(117, 32, 'The shoes were perfect! Love them! Shipping super fast\r\n', 3, '2016-11-12'),
(118, 32, 'The Presto sneaker that I purchased for my son is very stylish and he states are comfortable.', 3, '2016-12-19'),
(119, 33, 'I found this pair of shoes on the website and thought they looked awesome. I showed them to my 4 year old and he confirmed my thought. \r\nHe couldnt wait for our Nike shipment to arrive so he could wear his new kicks. I like them so much that I may have to get a pair of my own.\r\n', 3, '2016-02-02'),
(120, 33, 'The shoes are so comfortable. These are her school shoes and she love them. Thanks Nike!', 5, '0000-00-00'),
(121, 33, 'Purchase was quick and easy. Satisfied as usual with my purchase', 3, '2016-12-31'),
(122, 34, 'I love this company. Excellent service and products!!!\r\n', 5, '2016-04-20'),
(123, 34, 'I was very satisfied with the shoe for my son. I was also satisfied with the timely delivery of the shoe.\r\n', 5, '2016-06-11'),
(124, 34, 'I Love the look of this sneaker my only problem is the bubble when I wore them for the first time the left foot was making a \r\nnoise as if the air was coming out so i\'ll have to return them for a new pair and i also got them for my kids...', 3, '2016-07-21'),
(125, 35, 'I got these as a style shoe not for performance. I liked the color and they go good with everything', 2, '2016-08-11'),
(126, 35, 'He is 11 and LOVES his newJordans. He says they are very comfy.', 2, '2016-08-20'),
(127, 35, 'I got these as a style shoe not for performance....', 3, '2016-09-23'),
(128, 36, 'Love the shoes, great soles and the color way is gorgeous will definitely be buying from nike again either in store or online. \r\nshipping was prompt condition is better then I expected they made a great gift theres no question why my closet is filled with nike shoes', 4, '2016-10-20'),
(129, 36, 'Shoe looks great, material is durable. Colorway is eye catching, receiving compliments about it. Got it for a good price too.\r\n', 4, '2016-11-19'),
(130, 36, 'My son loves his new shoes. These are now his favorite shoes.', 5, '2016-11-24'),
(131, 37, 'great looking shoe\r\nID looks good fast shipping\r\nhaven\'t worn yet', 4, '2016-01-23'),
(132, 37, 'I have a fairly wide foot, and this is perfect! I am a beginner in basketball so I don\'t have much experience but this shoe is very comfortable, \r\nand I love wearing it all the time even while not playing basketball.', 4, '2016-01-31'),
(133, 37, 'Great traction. Great Fit. Great support. Great looking shoe.', 5, '2016-03-04'),
(134, 38, 'It is fast and durable they are coforatable. They look really cool. And they are just great shoes', 3, '2016-04-15'),
(135, 38, 'I love the shoe but I wish they came in half sizes a 4Y was a little too small but a 5Y was a little to big . I stuck with the 4Y because they will stretch \r\nover time but until then you can slightly see my toes through the shoe', 3, '2017-01-04'),
(136, 39, 'Very very comfortable sneaker. Chic and clean lines and great material for durability.', 5, '2017-01-13'),
(137, 39, 'It is very extreme i like and it fits me it is very comfortable', 5, '2017-02-19'),
(138, 40, 'Shoes are light and comfortable but laces are way too short cant keep them tied. Otherwise he loves them', 2, '2016-06-29'),
(139, 40, 'loved the fit and style of the shoe. will definitely recommend.', 4, '2017-04-14'),
(140, 40, 'My two year old loves these sneakers. We have purchased others with Velcro and have found that it wears out before the rest of the shoe. \r\nThis style is a great option and stays on my daughters feet really well.', 4, '2017-04-16'),
(141, 41, 'Its has good stile and plays basketball good. It also mathches well with a lot of things\r\n', 3, '2016-05-11'),
(142, 41, 'This is one of my favorite and best jordan sihlouettes. Its very comfortable and durable', 4, '2016-09-22'),
(143, 42, 'I have been wearing Air Jordan 1s since I was 11. Little did I know I would still love them 10 years later! Great shoe, \r\ncomfortable, stylish, I\'d buy these again.', 5, '2017-03-20'),
(144, 42, 'My daughter has had this shoe for three years (She doesn\'t grow very much!) and it\'s still going strong! Of course, it looks very worn out, but for three years, \r\nit is an amazing shoe! We will be repurchasing this item and the high tops in the future!', 4, '2017-04-02'),
(145, 42, 'These cool white black and red shoes are the equivalent of hot rods for your feet.\r\n', 4, '2017-05-01'),
(146, 43, 'Excellent quality. Got these for my daughter who has problems with her feet and she can walk a lot further with these.\r\n', 3, '2016-11-14'),
(147, 43, 'I have purchased both the zebra and tiger prints. I love both. The only problem is with the zebra color way, it is not truly black and white. \r\nThe zebra stripes are more brown than black.\r\n', 4, '2016-12-19'),
(148, 44, 'I bought this shoe for my daughter in high school. She loves them and they fit perfect and are very comfortable. I would definitely recommend this shoe!!\r\n', 5, '2016-06-04'),
(149, 44, 'I like the shoe material and the color it got. Its smoove and can match with almost anything.', 4, '2016-06-11'),
(150, 44, 'I really love these Nike please get my size in Mens 8.5 or 8 I will definitely buy more than one pair please help me by providing me with the best \r\ncustomer service support bless me 2017 happy new year', 5, '2016-07-16'),
(151, 45, 'The sneakers look even better in person beautiful all black sneakers !!!', 3, '2017-01-14'),
(152, 45, 'Ordered these for my niece for school who is a budding sneaker fanatic and she was beyond pleased, as was I. Typical fit for Jordan and definitely comfortable.\r\n', 4, '2017-02-10'),
(153, 46, 'Love love love! Very comfortable and eye catching!', 5, '2016-11-24'),
(154, 46, 'I absolutely love these. The way they set the color way to these was amazing. They fit just right they arrived on time. All around fantastic\r\n', 4, '2016-12-04'),
(155, 46, 'classic shoe that you cant go wrong with and great colors', 5, '2017-01-20'),
(156, 47, 'Great product. Was waiting on these release and I absolutely love them', 3, '2016-09-11'),
(157, 47, 'This shoe is very comfortable - looks very nice with the leather on it and reflective swoosh. Would recomend to many.\r\n', 3, '2016-10-03'),
(158, 48, 'I LOVE THIS COLORWAY IT REMINDS ME ABOUT THE PHX SUNS I ABSOLUTELY LOVE THESE SHOES! BUY THESE SHOES THERE AMAZING! #COOLEST KID ON THE BLOCK!\r\n', 4, '2017-02-04'),
(159, 48, 'This shoes runs a little smaller and narrower than other Jordans that I have. I recommend going a half size up.\r\n', 3, '2017-02-05'),
(160, 48, 'I bought these for my daughter. She absolutely loves them just like all his other Jordan sneakers. (Little sneaker head) The sneaker runs true to size. \r\nThe suede that was used it great but most of all the color is vibrant! Id recommend these other Nike/Jordan lovers! Happy Daughter,Happy Mom!!\r\n', 4, '2017-03-17'),
(161, 12, 'My two year old loves these sneakers. We have purchased others with Velcro and have found that it wears out before the rest of the shoe. This style is a great option and stays on my daughters feet really well.', 3, '2017-06-04'),
(162, 50, 'ghfdglhfgq', 3, '2017-06-05'),
(163, 1, 'perfect', 3, '2017-06-05'),
(164, 2, 'perfect', 3, '2017-06-05');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `pid` int(11) NOT NULL,
  `pname` varchar(30) NOT NULL,
  `pdetail` text NOT NULL,
  `pcost` decimal(10,0) NOT NULL,
  `pprice` double NOT NULL,
  `ptype` varchar(6) NOT NULL,
  `ppath` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`pid`, `pname`, `pdetail`, `pcost`, `pprice`, `ptype`, `ppath`) VALUES
(1, 'Racer ZZ14', 'The Nike Air Max 1 SE Women\'s Shoe reinvents the original with a synthetic and textile upper while retaining the revolutionary cushioning that made it famous.', '122', 180, 'MEN', 'e5bc6821-70a6-44ee-8d25-16e33d97f6b4.jpg'),
(2, 'Air Jordan 4', 'ICONIC STYLE\r\nOne of the most coveted shoe models gets a vintage lift in the Air Jordan 4 Retro Men\'s Shoe. With side ankle supports and innovative upper materials, this shoe delivers a supportive, comfortable fit.', '100', 190, 'MEN', '17e3f3cc-6479-4a1e-abe3-c2e17a99f818.jpg'),
(3, 'Nike Air Zoom', 'VERSATILE AND FAST\r\nThe latest Nike Air Zoom Pegasus 34 Women\'s Running Shoe features updated Flymesh fabric for exceptional breathability and lightweight, responsive cushioning that keeps you comfortable for miles.', '50', 150, 'WOMEN', 'c8f39fc8-01a8-4a8f-bec2-bc88ac79b5c9.jpg'),
(4, 'NIKE CORTEZ', 'Inspired by the 1972 icon, the Nike Cortez Basic SL Big Kids\' Shoe stays true to the original while providing durability and lightweight cushioning', '30', 120, 'BOYS', 'e0dc0320-93dd-49b3-8c5e-fcb293e8cf71.jpg'),
(5, 'Fight Bonafide', 'Nike Flyknit technology was inspired by feedback from runners seeking a shoe with the snug (and virtually unnoticed) fit of a sock. Nike embarked on a four-year mission with teams of programmers, engineers and designers to create the technology needed to make the knit upper with static properties for structure and durability. Then the precise placement of support, flexibility and breathability—all in one layer—was refined. The result is a featherweight, formfitting and virtually seamless upper. This unprecedented precision maximizes performance while reducing material waste by an average of 60% compared to traditional cut and sew, saving millions of pounds of material from reaching landfills.', '50', 150, 'MEN', '5.jpg'),
(6, 'Flyknit 2017', 'The Nike Free RN Motion Flyknit 2017 Men\'s Running Shoe features an incredibly flexible sole that lets you move naturally and a revolutionary two-strap system for snug, lace-free comfort.', '50', 150, 'MEN', '6.jpg'),
(7, 'Sock Dart', 'With an all-over Safari print updated with rich colors, the Nike Sock Dart QS Men\'s Shoe stands out from the pack. In addition to the iconic print first used on 1987\'s Air Safari shoe is the tried and true lightweight comfort of the Sock Dart for a bold look to an everyday style.', '40', 120, 'MEN', '7.jpg'),
(9, 'LeBron XIV', 'With a new midfoot strap that stretches with your foot, the LeBron XIV Men\'s Basketball Shoe is extremely light, flexible and streamlined for speed.', '55', 175, 'MEN', '9.jpg'),
(10, 'Air Max 2017', 'Seamlessly designed with support and breathability right where you need it, the Nike Air Max 2017 Men\'s Running Shoe features a Flymesh upper combined with the plush cushioning of a full-length Max Air unit.', '60', 190, 'MEN', '10.jpg'),
(11, 'Air Jordan 1 Retro', 'The Air Jordan 1 Retro High OG Men\'s Shoe delivers heritage style with comfortable leather and responsive cushioning.', '60', 160, 'MEN', '11.jpg'),
(12, 'Jordan Melo M13', 'The Jordan Melo M13 Men\'s Basketball Shoe offers lightweight stability, support and ultra-responsive cushioning designed for the evolution of Melo\'s game.', '40', 135, 'MEN', '12.jpg'),
(13, 'Air More Uptempo', 'A retro basketball sneaker based on the original released in the mid 90s, The Nike Air More Uptempo Little/Big Kids\' Shoe stands out with an audacious mix of performance comfort and expressive style.', '20', 130, 'BOYS', '13.jpg'),
(14, 'Little Posite One XX', 'The Nike Little Posite One XX Big Kids\' Shoe celebrates the 20th anniversary of the hoops-inspired icon with its unmistakable molded upper, lightweight cushioning and durable rubber outsole.', '80', 180, 'BOYS', '14.jpg'),
(15, 'Air Jordan 6 Retro', 'The Air Jordan 6 Retro Big Kids\' Shoe features a perforated leather upper and Air-Sole unit for ventilated comfort and the plush cushioning that made it famous.', '60', 140, 'BOYS', '15.jpg'),
(56, 'test', 'test', '123', 456, 'MEN', '57efe9d2-2c0d-4993-8ce3-52b16409a737.jpg'),
(57, 'Classic Cortez Premium', 'The Nike Classic Cortez Premium Men\'s Shoe brings back the famous, old-school runner with an updated construction for a premium look.', '50', 90, 'MEN', '111a3ba3-5b43-4574-b128-1551646bbed9.jpg'),
(17, 'Air Max 90 Ultra 2', 'The Nike Air Max 90 Ultra 2.0 BR Big Kids\' Shoe transforms the iconic 90s sneaker by significantly reducing its weight, helping you stay quick and comfortable from the first bell to the last', '20', 110, 'BOYS', '17.jpg'),
(18, 'Air Jordan 9 Retro', 'The Air Jordan 9 Retro Big Kids\' Shoe is influenced by the beauty of Japan. Lightweight cushioning offers a comfortable fit, and multilingual details at the sole list the ingredients that make up a game-changer.', '60', 140, 'BOYS', '18.jpg'),
(19, 'Zoom Witness', 'The Nike Zoom Witness Big Kids\' Basketball Shoe features Featherposite construction—an innovative upper technology that provides lightweight durability for long-lasting play on the court', '10', 90, 'BOYS', '19.jpg'),
(20, 'Chuck Taylor All Star', 'The Converse Chuck Taylor All Star Simple Step Americana makes getting in and out of shoes a snap with a convenient, hook-and-loop side closure.', '10', 45, 'BOYS', '20.jpg'),
(21, 'Jordan Formula 23', 'Featuring a lightweight, deconstructed design, the Jordan Formula 23 Big Kids\' Shoe takes its cues from the Air Jordan X for a modern take on an all-time classic.', '20', 90, 'BOYS', '21.jpg'),
(22, 'Air Max Tavas', 'The Nike Air Max Tavas Big Kids\' Shoe offers springy cushioning and lightweight support to help you stay quick and comfortable at school or at play. A close-fitting textile upper provides breathability and a sleek, modern look.', '40', 85, 'BOYS', '22.jpg'),
(23, 'Jordan Eclipse', 'Featuring a premium textile upper in a lightweight, streamlined silhouette, the Jordan Eclipse Chukka Big Kids\' Shoe is designed for an elevated look with subtle details that carry through the Jordan legacy.', '20', 90, 'BOYS', '23.jpg'),
(24, 'Kyrie 3', 'The Kyrie 3 Big Kids\' Basketball Shoe helps you move quickly in any direction thanks to a uniquely rounded outsole.', '20', 90, 'BOYS', '24.jpg'),
(25, 'Air Max 1 SE', 'The Nike Air Max 1 SE Women\'s Shoe reinvents the original with a synthetic and textile upper while retaining the revolutionary cushioning that made it famous.', '50', 130, 'WOMEN', '25.jpg'),
(26, 'Lunar Empress 2', 'Made with a no-sew upper and Dynamic Flywire support, the Nike Lunar Empress 2 Women\'s Golf Shoe helps keep you comfortable while locking down the foot with an adaptive fit.', '50', 120, 'WOMEN', '26.jpg'),
(27, 'LD Runner', 'The Nike LD Runner Women\'s Shoe delivers lightweight breathability and plush, supportive cushioning.', '25', 75, 'WOMEN', '27.jpg'),
(28, 'Tennis Classic Ultra', 'The Nike Tennis Classic Ultra Premium Women\'s Shoe celebrates a retro sneaker hero with a sleek redesign, amping up your stride with a light, cushioned feel.', '50', 120, 'WOMEN', '28.jpg'),
(29, 'Flex Experience RN', 'The Nike Flex Experience RN 6 Premium Women\'s Running Shoe helps keep you light on your feet from start to finish with a minimal mesh upper and super-flexible outsole.', '10', 70, 'WOMEN', '29.jpg'),
(30, 'Chuck Taylor High ', 'The Converse Chuck Taylor Americana puts an American flag-inspired canvas upper onto our iconic high top. This is one of our most popular alternative Chucks.', '10', 60, 'WOMEN', '30.jpg'),
(31, 'Air Max 90 Ultra 2.0', 'The Nike Air Max 90 Ultra 2.0 Women\'s Shoe updates an iconic profile with premium construction for lasting comfort.', '40', 120, 'WOMEN', '31.jpg'),
(32, 'ir Max 90 Ultra 2.0', 'The Nike Air Max 90 Ultra 2.0 Flyknit Metallic Women\'s Shoe elevates the iconic profile with a flexible, breathable Flyknit upper and the comfortable cushioning that made it a favorite.', '100', 200, 'WOMEN', '32.jpg'),
(33, 'Metcon Repper DSX', 'Over any distance and during every activity, the Nike Metcon Repper DSX Women\'s Training Shoe is designed to withstand the demands of cross-training and keep you moving at full speed.', '50', 100, 'WOMEN', '33.jpg'),
(34, 'Blazer Premium', 'The Nike Blazer Premium Women\'s Golf Shoe combines an iconic design with premium, weatherproof material and an innovative, spikeless traction pattern for stability on the course.', '50', 190, 'WOMEN', '34.jpg'),
(35, 'HypervenomX 3', 'The Nike HypervenomX Phelon 3 Indoor/Court Soccer Shoe features an anatomical fit and large strike zone area for unrivaled agility and better ball control on the street, court and indoor surfaces.', '10', 80, 'WOMEN', '35.jpg'),
(36, 'Chuck Taylor Star', 'The Converse Chuck Taylor All Star DC Comics Wonder Woman celebrates the iconic superheroine with bold graphics on the classic All Star profile.', '20', 60, 'WOMEN', '36.jpg'),
(37, 'PG 1', 'Created for Paul George\'s versatile playing style, the Nike PG 1 Big Kids\' Basketball shoe offers a locked-in fit and durable traction to help you move quickly and confidently in any direction.', '20', 60, 'GIRLS', '37.jpg'),
(38, 'LeBron XIV', 'Inspired by the explosive play of LeBron James, the fully updated LeBron XIV Big Kids\' Basketball Shoe delivers springy cushioning and a supportive, flexible fit.', '40', 140, 'GIRLS', '38.jpg'),
(39, 'Presto BR', 'The Nike Presto BR Big Kids\' Shoe is made with lightweight, breathable mesh for everyday comfort.', '40', 100, 'GIRLS', '39.jpg'),
(40, 'Presto Extreme', 'Designed to feel as comfy as your favorite tee, the Nike Presto Extreme Big Kids\' Shoe offers distinctive, adaptive style and great flexibility.', '40', 90, 'GIRLS', '40.jpg'),
(41, 'Air Max 90 Print L', 'The Nike Air Max 90 Print Leather Big Kids\' Shoe reimagines an icon with a bold design while delivering the incredible cushioning that made the original famous.', '40', 95, 'GIRLS', '41.jpg'),
(42, 'Roshe One Print', 'The Nike Roshe One Print Big Kids\' Shoe offers breathability and lightweight cushioning with a colorful mesh upper and Phylon midsole. The shoe is intended to be versatile, worn with or without socks, dressed up or down, for walking or just taking it easy.', '40', 90, 'GIRLS', '42.jpg'),
(43, 'Air Max 2017', 'The lightweight Nike Air Max 2017 Big Kids\' Running Shoe delivers flexible, full-length cushioning that you\'ll feel—and see—on every run.', '40', 90, 'GIRLS', '43.jpg'),
(44, 'Flex RN 2017', 'The Nike Flex RN 2017 Big Kids\' Running Shoe offers breathability, support and flexibility for comfort over any distance.', '40', 90, 'GIRLS', '44.jpg'),
(45, 'Air Max 90 Leather', 'The Nike Air Max 90 Leather Big Kids\' Shoe features a leather upper and a visible Max Air unit in the heel for durability and outstanding cushioning.', '40', 90, 'GIRLS', '45.jpg'),
(46, 'Chuck Taylor Star A', 'The Converse Chuck Taylor All Star Street Americana features a patriotic print and cushioned collar for an iconic look and lasting comfort', '10', 45, 'GIRLS', '46.jpg'),
(47, 'Little Posite Pro', 'The Nike Little Posite Pro Big Kids. Shoe features stitch-free construction and a sleek, translucent sole for durable comfort and lightweight cushioning.', '50', 180, 'GIRLS', '47.jpg'),
(48, 'Air Jordan 13 Retro', 'The Air Jordan 13 Retro Big Kids Shoe updates the original with reflective details and a suede and woven upper for support and comfort.', '50', 140, 'GIRLS', '48.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `shoppingcart`
--

CREATE TABLE `shoppingcart` (
  `cartid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` double NOT NULL,
  `ucid` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `uid` int(11) NOT NULL,
  `fullname` varchar(30) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `type` varchar(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`uid`, `fullname`, `username`, `password`, `email`, `type`) VALUES
(1, 'Ashok Kumara Perera', 'ashok', 'ashok@123', 'ashokkumara20@gmail.com', 'ADMIN'),
(2, 'Chrishan Perera', 'chrishan', 'chrishan@123', 'pererachrishan729@gmail.com', 'USER'),
(3, 'Nirmana Pelendagama', 'nirmana', 'nirmana@123', 'nirmana666@gmail.com', 'USER'),
(4, 'Anjana Silva', 'anjana', 'anjana@123', 'anjana@gmail.com', 'USER'),
(5, 'Duwane Wanigasinhe', 'duwane', 'duwane@123', 'duwane@gmail.com', 'USER'),
(6, 'test perera', 'test', 'test123', 'test@gmail.com', 'USER');

-- --------------------------------------------------------

--
-- Table structure for table `usercart`
--

CREATE TABLE `usercart` (
  `ucid` int(11) NOT NULL,
  `uid` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`cid`),
  ADD KEY `pid` (`pid`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `shoppingcart`
--
ALTER TABLE `shoppingcart`
  ADD PRIMARY KEY (`cartid`),
  ADD KEY `FK_UserCartId` (`ucid`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `usercart`
--
ALTER TABLE `usercart`
  ADD PRIMARY KEY (`ucid`),
  ADD KEY `FK_UserId` (`uid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=165;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `shoppingcart`
--
ALTER TABLE `shoppingcart`
  MODIFY `cartid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `usercart`
--
ALTER TABLE `usercart`
  MODIFY `ucid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
